using RenderHeads.Media.AVProVideo;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class RadioSelect : MonoBehaviour
{
    [SerializeField] private Toggle chk;
    [SerializeField] private Slider sld;
    private MediaPlayer audioSource;
    private string[] radios = {"https://playerservices.streamtheworld.com/api/livestream-redirect/CRP_MOD_SC?csegid=30008&dist=30008",
        "https://us-b4-p-e-az1-audio.cdn.mdstrm.com/live-audio-aw/5fada54116646e098d97e6a5/playlist.m3u8?listeningSessionID=619482cf81feae2a_841329_jcTB4sVD__0000002KJsA&downloadSessionID=0&aid=5faaeb72f92d7b07dfe10181&pid=xY35nHr0SVXuFhQ4PvTtaV9YYmhoP3ef&sid=u2YmDbqpUzUKbln4r0FNJBWRPx9emPqx&uid=rURbAiMeXADjNl5aScfkfzmPZRjPqnPJ&es=us-b4-p-e-az1-audio.cdn.mdstrm.com&ote=1638050447401&ot=KtpOX1DhS_3DoDzvPe6cYA&proto=https&pz=us&cP=128000&awCollectionId=5faaeb72f92d7b07dfe10181&liveId=5fada54116646e098d97e6a5&referer=https%3A%2F%2Fwww.radiosdelperu.pe%2F",
        "https://playerservices.streamtheworld.com/api/livestream-redirect/CRP_OAS_SC?csegid=30008&dist=30008",
        "https://playerservices.streamtheworld.com/api/livestream-redirect/CRP_PLA_SC?csegid=30008&dist=30008"};
    // Start is called before the first frame update
    private void Awake()
    {
        audioSource = GetComponent<MediaPlayer>();
        playModa();
        PlayerPrefs.SetInt("isMuted", 0);
        PlayerPrefs.SetFloat("volumen", 1);
    }
    void Start()
    {
        chk.onValueChanged.AddListener(delegate { mutePlayer(chk); });
        sld.onValueChanged.AddListener(delegate { changeAudio(sld); });
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey("1"))
        {
            SceneManager.LoadScene("Juego1", LoadSceneMode.Single);
        }
        if (Input.GetKey("2"))
        {
            SceneManager.LoadScene("Juego2", LoadSceneMode.Single);
        }
        if (Input.GetKey("3"))
        {
            SceneManager.LoadScene("Juego3", LoadSceneMode.Single);
        }
    }

    void changeAudio(Slider t)
    {
        audioSource.Control.SetVolume(t.value);
        PlayerPrefs.SetFloat("volumen", t.value);
    }

    void mutePlayer(Toggle t)
    {
        audioSource.Control.MuteAudio(t.isOn);
        if (t.isOn)
        {
            PlayerPrefs.SetInt("isMuted", 1);
        }
        else
        {
            PlayerPrefs.SetInt("isMuted", 0);
        }
    }

    public void playModa()
    {
        audioSource.OpenVideoFromFile(MediaPlayer.FileLocation.AbsolutePathOrURL, radios[0], true);
        PlayerPrefs.DeleteKey("path");
        PlayerPrefs.SetString("path", radios[0]);
    }

    public void playZona()
    {
        audioSource.OpenVideoFromFile(MediaPlayer.FileLocation.AbsolutePathOrURL, radios[1], true);
        PlayerPrefs.DeleteKey("path");
        PlayerPrefs.SetString("path", radios[1]);
    }

    public void playOasis()
    {
        audioSource.OpenVideoFromFile(MediaPlayer.FileLocation.AbsolutePathOrURL, radios[2], true);
        PlayerPrefs.DeleteKey("path");
        PlayerPrefs.SetString("path", radios[2]);
    }

    public void playPlaneta()
    {
        audioSource.OpenVideoFromFile(MediaPlayer.FileLocation.AbsolutePathOrURL, radios[3], true);
        PlayerPrefs.DeleteKey("path");
        PlayerPrefs.SetString("path", radios[3]);
    }

}
