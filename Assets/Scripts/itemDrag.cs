using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class itemDrag : MonoBehaviour,IDragHandler,IEndDragHandler,IBeginDragHandler
{
    [HideInInspector] public static GameObject item;
    Vector2 starPos;
    public Transform startDrag, endDrag;
    public void OnBeginDrag(PointerEventData eventData)
    {
        item = gameObject;
        starPos = transform.position;
        startDrag = transform.parent;
        transform.SetParent(endDrag);
    }

    public void OnDrag(PointerEventData eventData)
    {
        transform.position = Input.mousePosition;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        item = null;
       if(transform.parent == endDrag)
        {
            transform.position = starPos;
            transform.SetParent(startDrag);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey("1"))
        {
            SceneManager.LoadScene("Juego1", LoadSceneMode.Single);
        }
        if (Input.GetKey("2"))
        {
            SceneManager.LoadScene("Juego2", LoadSceneMode.Single);
        }
        if (Input.GetKey("3"))
        {
            SceneManager.LoadScene("Juego3", LoadSceneMode.Single);
        }
    }
}
