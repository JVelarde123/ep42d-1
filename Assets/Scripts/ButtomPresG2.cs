using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ButtomPresG2 : MonoBehaviour
{
    private string aux = "";
    private AudioSource source;
    [SerializeField] private InputField txt;
    private void Awake()
    {
        txt.enabled = false;
        source = GetComponent<AudioSource>();
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey("1"))
        {
            SceneManager.LoadScene("Juego1", LoadSceneMode.Single);
        }
        if (Input.GetKey("2"))
        {
            SceneManager.LoadScene("Juego2", LoadSceneMode.Single);
        }
        if (Input.GetKey("3"))
        {
            SceneManager.LoadScene("Juego3", LoadSceneMode.Single);
        }
    }

    public void pressButtom(string number)
    {
        source.Play();
        aux += number;
        txt.text = aux;
    }

    public void pressBad()
    {
        txt.text = "";
        aux = "";
    }

    public void pressButtomOk()
    {
        source.Play();
        if (txt.text.Equals("256983210457"))
        {
            colorEdit(Color.green);
        }
        else
        {
            StartCoroutine(animColor());
            txt.text = "";
            aux = "";
        }
    }
    private IEnumerator animColor()
    {
        colorEdit(Color.red);
        yield return new WaitForSeconds(0.3f);
        colorEdit(Color.white);
        yield return new WaitForSeconds(0.3f);
        colorEdit(Color.red);
        yield return new WaitForSeconds(0.3f);
        colorEdit(Color.white);
        yield return new WaitForSeconds(0.3f);
        colorEdit(Color.red);
        yield return new WaitForSeconds(0.3f);
        colorEdit(Color.white);

    }
    private void colorEdit(Color color)
    {

        var img1 = txt.GetComponent<Image>();
        img1.color = color;
    }
}
