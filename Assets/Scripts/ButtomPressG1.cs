using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ButtomPressG1 : MonoBehaviour
{
    [SerializeField] private Button b1, b2, b3, b4, b5, b6, b7, b8, b9,b10;
    private int aux = 0;
    private AudioSource source;
    private void Awake()
    {
        source = GetComponent<AudioSource>();
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey("1"))
        {
            SceneManager.LoadScene("Juego1", LoadSceneMode.Single);
        }
        if (Input.GetKey("2"))
        {
            SceneManager.LoadScene("Juego2", LoadSceneMode.Single);
        }
        if (Input.GetKey("3"))
        {
            SceneManager.LoadScene("Juego3", LoadSceneMode.Single);
        }
    }

    public void pressButtom(int number)
    {
        aux++;
        if (aux == number)
        {
            btnGood(number);
        }else
        {
            badColors();
        }
        source.Play();
    }

    private void btnGood(int i)
    {
        switch (i)
        {
            case 1:
                changeColorG(b1);
                break;
            case 2:
                changeColorG(b2);
                break;
            case 3:
                changeColorG(b3);
                break;
            case 4:
                changeColorG(b4);
                break;
            case 5:
                changeColorG(b5);
                break;
            case 6:
                changeColorG(b6);
                break;
            case 7:
                changeColorG(b7);
                break;
            case 8:
                changeColorG(b8);
                break;
            case 9:
                changeColorG(b9);
                break;
            case 10:
                changeColorG(b10);
                break;
        }
    }

    private void changeColorG(Button b)
    {
        var image = b.GetComponent<Image>();
        image.color = Color.green;
    }

    private void badColors()
    {
        aux = 0;
        StartCoroutine(animColor());
    }

    private IEnumerator animColor()
    {
        colorEdit(Color.red);
        yield return new WaitForSeconds(0.3f);
        colorEdit(Color.white);
        yield return new WaitForSeconds(0.3f);
        colorEdit(Color.red);
        yield return new WaitForSeconds(0.3f);
        colorEdit(Color.white);
        yield return new WaitForSeconds(0.3f);
        colorEdit(Color.red);
        yield return new WaitForSeconds(0.3f);
        colorEdit(Color.white);
        
    }

    private void colorEdit(Color color)
    {
        
        var img1 = b1.GetComponent<Image>();
        var img2 = b2.GetComponent<Image>();
        var img3 = b3.GetComponent<Image>();
        var img4 = b4.GetComponent<Image>();
        var img5 = b5.GetComponent<Image>();
        var img6 = b6.GetComponent<Image>();
        var img7 = b7.GetComponent<Image>();
        var img8 = b8.GetComponent<Image>();
        var img9 = b9.GetComponent<Image>();
        var img10 = b10.GetComponent<Image>();

        img1.color = color;
        img2.color = color;
        img3.color = color;
        img4.color = color;
        img5.color = color;
        img6.color = color;
        img7.color = color;
        img8.color = color;
        img9.color = color;
        img10.color = color;
    }
}
