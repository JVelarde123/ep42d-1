using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class itemDrop : MonoBehaviour,IDropHandler
{
    [HideInInspector] public GameObject item;
    [SerializeField] private AudioSource audioSource;
    [SerializeField] private string name;
    public void OnDrop(PointerEventData eventData)
    {
        if(itemDrag.item.name == name)
        if (!item)
        {
            audioSource.Play();
            item = itemDrag.item;
            item.transform.SetParent(transform);
            item.transform.position = transform.position;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(item!= null && item.transform.parent != transform)
        {
            item = null;
        }
    }
}
