using RenderHeads.Media.AVProVideo;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadioOnGame : MonoBehaviour
{
    private MediaPlayer audioSource;
    // Start is called before the first frame update
    private void Awake()
    {
        audioSource = GetComponent<MediaPlayer>();
        audioSource.OpenVideoFromFile(MediaPlayer.FileLocation.AbsolutePathOrURL, PlayerPrefs.GetString("path"), true);
        audioSource.Control.SetVolume(PlayerPrefs.GetFloat("volumen"));
        if (PlayerPrefs.GetInt("isMuted") == 0) { 
        audioSource.Control.MuteAudio(false);
        } else
        {
            audioSource.Control.MuteAudio(true);
        }
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
